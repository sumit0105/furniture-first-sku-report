//
//  America'sSleepVC.swift
//  Furniture First SKU Report
//
//  Created by Yes IT Labs  on 04/01/22.
//

import UIKit
import WebKit

class America_sSleepVC: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://furniturefirst.coop/survey/americas-sleep-specialists-sku-inventory-report-2019/")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    


}
