//
//  MattressVC.swift
//  Furniture First SKU Report
//
//  Created by Yes IT Labs  on 04/01/22.
//

import UIKit
import WebKit

class MattressVC: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://furniturefirst.coop/survey/mattress-1st-sku-inventory-report-mattress-1st-2019/")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    

    
}
