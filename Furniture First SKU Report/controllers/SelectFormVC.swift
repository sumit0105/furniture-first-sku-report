//
//  SelectFormVC.swift
//  Furniture First SKU Report
//
//  Created by Yes IT Labs  on 04/01/22.
//

import UIKit

class SelectFormVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
     
    @IBAction func MattressBtn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MattressVC") as! MattressVC
               self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func AmericasSleepBtn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "America_sSleepVC") as! America_sSleepVC
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
